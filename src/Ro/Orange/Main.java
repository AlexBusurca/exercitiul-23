package Ro.Orange;

import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Item> items = Arrays.asList(
	      new Item("Towel",156),
          new Item("Sheet",75)
        );

        int quantity = items.stream()
                       .mapToInt(q -> q.getQuantity())
                       .sum();

        System.out.println(quantity);
    }
}
